package luise.faculdade.ia.trabalho.model;

public class Configuracoes {
	  private int origem, destino;
	  private int tamanhoPopulacao;
	  private double taxaCruzamento;
	  private int taxaMutacao;
	  private int mecanismoSelecao;
	  private int numeroGeracoes;

	  public Configuracoes (int origem, int destino, int tamanhoPopulacao, double taxaCruzamento, int taxaMutacao, int mecanismoSelecao, int numeroGeracoes ) {
	    this.origem = origem;
		this.destino = destino;  
		this.tamanhoPopulacao = tamanhoPopulacao;
	    this.taxaCruzamento = taxaCruzamento;
	    this.taxaMutacao = taxaMutacao;
	    this.mecanismoSelecao = mecanismoSelecao;
	    this.numeroGeracoes = numeroGeracoes;
	  }

	  public int getOrigem() {
		return origem;
	  }
	  public int getDestino() {
		return destino;
	  }
	  public int getTamanhoPopulacao() {
	    return tamanhoPopulacao;
	  }
	  public double getTaxaCruzamento() {
	    return taxaCruzamento;
	  }
	  public int getTaxaMutacao() {
	    return taxaMutacao;
	  }
	  public int getMecanismoSelecao() {
	    return mecanismoSelecao;
	  }
	  public int getNumeroGeracoes() {
	    return numeroGeracoes;
	  }
}