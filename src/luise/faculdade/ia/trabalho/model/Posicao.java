package luise.faculdade.ia.trabalho.model;

public class Posicao {
	  private int pos;
	  private int fitness;
	  public Posicao (int pos, int fitness) {
	    this.pos = pos;
	    this.fitness = fitness;
	  }
	  public void SetPos (int pos){
	    if (pos > 0 && pos < 100 ) {
	      this.pos = pos;
	    } else {this.pos = -1;}
	  }
	  public void SetFitness (int fitness) {
	    if (fitness > 0) {
	      this.fitness = fitness;
	    } else {this.fitness = -1;}
	  }
	  public int GetPos() {
	    return pos;
	  }
	  public int GetFitness() {
	    return fitness;
	  }
}