package luise.faculdade.ia.trabalho.model;


public class Caminho {
	private Posicao[] caminho;
	private int[] camint;
	private int fitness;
	
	public Caminho (int tamanho) {
		camint = new int[tamanho];
		caminho = new Posicao[tamanho];
		for (int i = 0; i < tamanho; i++) {
			caminho[i] = new Posicao(0, 0);
		}
	}
	
	public void setPosicao(Posicao pos, int indice) {
		caminho[indice] = pos;
	}
	
	public void setCaminho(Posicao[] pos) {
		caminho = pos;
	}
	
	public void setFitness(int fitness) {
		if (fitness > 0) {
			this.fitness = fitness;
		} else {this.fitness = -1;}
	}
	
	public Posicao getPosicao(int indice) {
		return caminho[indice];
	}
	
	public Posicao[] getCaminho() {
		return caminho;
	}
	
	public int[] getCamint() {
		return camint;
	}
	
	public int getFitness() {
		return fitness;
	}
	
	public void geraCamint() {
		int i = 0;
		for (Posicao aux : caminho) {
			camint[i] = aux.GetPos();
		}
	}
}
