package luise.faculdade.ia.trabalho.gui;

import luise.faculdade.ia.trabalho.model.*;
import luise.faculdade.ia.trabalho.util.*;

import java.util.Arrays;
import java.util.Scanner;

public class GUI {
	private Caminho caminhos[];
	private Configuracoes config;
	private Util util;
	private int tamanho, fit[];
	
	public void Teste() {		
		int origem, destino;
		util = new Util();
		Scanner scan = new Scanner(System.in);
		
		System.out.println("C�lculo do melhor caminho. Primeiro, entre com a quantidade de caminhos desejados:");
		tamanho = scan.nextInt();
		
		do {
			System.out.println("Agora entre com o campo desejado para a origem: ");
			origem = scan.nextInt();
			System.out.println("Entre com o campo desejado para o destino: ");
			destino = scan.nextInt();
		} while (origem > 100 || destino > 100);
		scan.close();
		
		config = new Configuracoes(origem, destino, tamanho, 0, 0, 0, 0);
		
		caminhos = new Caminho[config.getTamanhoPopulacao()];
		fit = new int[config.getTamanhoPopulacao()];
		
		System.out.println("inicializando matriz...");
		caminhos = util.matriz(caminhos);
		
		caminhos = util.setDestinoOrigem(caminhos, config.getOrigem(), config.getDestino());
		caminhos = util.geraFitness(caminhos);
		
		System.out.println("Calculando fitness e ordenando...");
		fit = util.calculaFitness(caminhos, fit);
		Arrays.sort(fit);
		
		System.out.println("Fitness obtido: ");
		for (int j : fit) {
			System.out.print(" " + j);
		}
		System.out.println("");
	}
}
