package luise.faculdade.ia.trabalho.util;

import luise.faculdade.ia.trabalho.model.*;

public class Fitness {
	  public static int Calculo (Caminho caminho) {
		  int calc = 0;
		  for (int i = 0; i < caminho.getCaminho().length-1; i++) {
			  calc += Math.abs(caminho.getPosicao(i).GetPos() - caminho.getPosicao(i+1).GetPos())+1;
		  }
	    return (calc);
	  }
}
