package luise.faculdade.ia.trabalho.util;

import luise.faculdade.ia.trabalho.model.Caminho;
import luise.faculdade.ia.trabalho.model.Posicao;

public class Util {
	private Filler filler;
	private int matriz = 100;
	
	public Caminho[] matriz(Caminho[] caminhos) {
		filler = new Filler();
		for (int i = 0; i < caminhos.length; i++) {
			caminhos[i] = new Caminho(matriz);
			caminhos[i] = filler.geraPopulacao(caminhos[i], matriz);
		}
		return caminhos;
	}
	
	public Caminho[] setDestinoOrigem(Caminho[] caminhos, int origem, int destino) {
		for (int i = 0; i < caminhos.length; i++) {
			caminhos[i].setPosicao(new Posicao(origem, 0), 0);
			caminhos[i].setPosicao(new Posicao(destino, 0), matriz-1);
		}
		return caminhos;
	}
	
	public Caminho[] geraFitness(Caminho[] caminhos) {
		for (int i = 0; i < caminhos.length; i++) {
			caminhos[i].setFitness(Fitness.Calculo(caminhos[i]));
		}
		return caminhos;
	}
	
	public int[] calculaFitness(Caminho[] caminhos, int fit[]) {
		for (int i = 0; i < caminhos.length; i++) {
			fit[i] = caminhos[i].getFitness();
		}
		return fit;
	}
}
