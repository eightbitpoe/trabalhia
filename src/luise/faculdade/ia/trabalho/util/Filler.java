package luise.faculdade.ia.trabalho.util;

import luise.faculdade.ia.trabalho.model.Caminho;
import luise.faculdade.ia.trabalho.model.Posicao;

import java.util.Random;

public class Filler {
	private Random numero = new Random();
	
	public Caminho geraPopulacao(Caminho caminho, int tam) {
		Caminho auxiliar = caminho;
		for (Posicao aux : auxiliar.getCaminho()) {
			aux.SetPos(numero.nextInt(tam));
		}
		return auxiliar;
	}
}
